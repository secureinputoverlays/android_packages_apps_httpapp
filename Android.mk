LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := samples

# Only compile source java files in this apk.
LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := HTTPApp

LOCAL_SDK_VERSION := current

LOCAL_STATIC_JAVA_LIBRARIES := volley jebpf

include $(BUILD_PACKAGE)

