/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sobel.android.httpapp;




import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.regex.Pattern;

import android.app.Activity;

import com.sobel.android.attestedhttpservice.AttestedPost;
import com.sobel.android.attestedhttpservice.AttestedPostResponse;
import com.sobel.android.hiddenbufferservice.HiddenBufferHandle;
import com.sobel.android.hiddenbufferservice.HiddenBufferManager;
import com.sobel.android.hiddenbufferservice.HiddenContentHandle;
import com.sobel.android.hiddenbufferservice.HiddenContentHandle.QueryError;
import com.sobel.android.tpmsigningservice.TPMSigningClient;
import com.sobel.jebpf.EBPFInstruction;
import com.sobel.jebpf.EBPFInstruction.Register;

import android.os.Bundle;
import android.os.IBinder;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Activity For Application Attested HTTP
 */
public class HTTPAppActivity extends Activity {
    /**
     * Called with the activity is first created.
     */

	private TextView mText;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the layout for this activity.  You can find it
        // in res/layout/hello_activity.xml
        View view = getLayoutInflater().inflate(R.layout.http_app_activity, null);
        setContentView(view);

        mText = (TextView) findViewById(R.id.text);
        mText.setMovementMethod(new ScrollingMovementMethod());
    }
    
    public void handleClick(View v) {
    	doHttpRequest();
    }
    
    private void doHttpRequest() {
    	
    	logToScreen("Creating and filling hidden buffer..");
    	HiddenBufferManager m = new HiddenBufferManager();
    	HiddenBufferHandle ccHandle = m.newBuffer(); // TODO: only system can be allowed to do this!
    	m.setBufferText(ccHandle.mPrivateToken, "test");
    	IBinder cc1 = m.createSnapshot(ccHandle.mPublicToken);

    	HiddenContentHandle hbhcc1 = new HiddenContentHandle(cc1);
    	// Run some queries
    	hbhcc1.getLength();
    	hbhcc1.matchesPattern(Pattern.compile("bloop"));
    	hbhcc1.matchesPattern(Pattern.compile("^\\d"));
    	hbhcc1.runEBPF(EBPFInstruction.encodeMany(new EBPFInstruction[] {
    		EBPFInstruction.MOV_IMM(Register.R0, 0),
    		EBPFInstruction.EXIT(),
    	}));
    	
    	m.setBufferText(ccHandle.mPrivateToken, "123-444 fhhd");
    	IBinder cc2 = m.createSnapshot(ccHandle.mPublicToken);
    	HiddenContentHandle hbhcc2 = new HiddenContentHandle(cc2);
    	
    	HiddenBufferHandle pwHandle = m.newBuffer(); // TODO: only system can be allowed to do this!
    	m.setBufferText(pwHandle.mPrivateToken, "12345abcd");
    	IBinder ppw = m.createSnapshot(pwHandle.mPublicToken);
    	final HiddenContentHandle hbhpw = new HiddenContentHandle(ppw);
    	hbhpw.matchesPattern(Pattern.compile(".*?a"));
    	
    	String url = "http://10.0.2.2:9876/httpapp";

    	AttestedPost ah = new AttestedPost.Builder()
    		.url(url)
    		.addHeader("foo", "bar")
    		.addHeader("hmmm", "hawwww")
    		.addParam("first", "louis")
    		.addParam("last", "sobel")
    		.addParam("cc1", hbhcc1)
    		.addParam("cc2", hbhcc2)
    		.addParam("pw", hbhpw)
    		.build();
    	
    	logToScreen("Doing post...");
    	ah.perform(new AttestedPost.ResponseListener() {
			@Override
			public void onResponse(AttestedPostResponse r) {
				logToScreen("Got response: (" + r.getCode() + ") " + r.getStatusLine());
				Iterator<String> it = r.getHeaders().keySet().iterator();
				while (it.hasNext()) {
					String h = it.next();
					logToScreen(" > " + h + ": " + r.getHeaders().get(h));
				}
				try {
					logToScreen(new String(r.getBody(), "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					logToScreen("UTF-8 doesn't exist - uh oh.");
				}
				
		    	// Query after request should cause error if 200
		    	logToScreen("Doing query after request,,, " + r.getCode());
		    	boolean queryFailed = false;
		    	try {
		    		hbhpw.getLength();
		    	} catch (HiddenContentHandle.QueryError e) {
		    		queryFailed = true;
		    	}

		    	boolean expectedFailure = (r.getCode() != 409);
		    	if (expectedFailure) {
		    		if (queryFailed) {
			    		logToScreen("Caused error: good");
		    		} else {
		    			logToScreen("UHOH: Query should have failed....!>!>!");
		    		}
		    	} else {
		    		if (queryFailed) {
		    			logToScreen("UHOH: Query shouhld have succeeded");
		    		} else {
		    			logToScreen("Didn't fail: good");
		    		}
		    		
		    	}
				
		    	// This should fail no matter what
		    	sendSomewhereElse(hbhpw);
				
			}
		}, new AttestedPost.ErrorListener() {
			@Override
			public void onError(String msg) {
				logToScreen("Got error: " + msg);
				
		    	// Query after error should fail!
				// Well, actually, it depends when the error happened.
				// It is is before request, shouldn't fail.
				// (but also cannot unlock buffer)
		    	logToScreen("Doing query after error request...");
		    	boolean queryFailed = false;
		    	try {
		    		hbhpw.getLength();
		    	} catch (HiddenContentHandle.QueryError e) {
		    		queryFailed = true;
		    		logToScreen("Caused error: good");
		    	}
		    	if (!queryFailed) {
		    		logToScreen("UHOH: Query should have failed....!>!>!");
		    	}
		    	
		    	// And this also depends on whether it was ever
		    	// assocated. In general, failures leave us in secure,
		    	// but messy state.
		    	sendSomewhereElse(hbhpw);
			}
		});

    }
    
    private void sendSomewhereElse(HiddenContentHandle p) {
		// Alright, now try to send it somewhere else..
    	logToScreen("Trying to send it somehwere else now...");
		String url2 = "http://10.0.2.2:9876/httpapp_2";
		AttestedPost ah2 = new AttestedPost.Builder()
			.url(url2)
			.addParam("pw", p)
			.build();
		ah2.perform(new AttestedPost.ResponseListener() {
			@Override
			public void onResponse(AttestedPostResponse r) {
				logToScreen("UHOH post to different URL worked");
			}
		}, new AttestedPost.ErrorListener() {
			@Override
			public void onError(String msg) {
				logToScreen("Post to someone else failed!: " + msg);
			}
		});
    }
    
    private void doQuote() {
    	logToScreen("Quoting Data...");
    	TPMSigningClient t = new TPMSigningClient();
    	byte[] data = new byte[] {0,1,2,3,4,5};
    	byte[] nonce = new byte[] {(byte)0xC0, (byte)0xFF, (byte)0xEE};
    	byte[] sig = t.quoteData(data, nonce);
    	logToScreen("  sig: " + Base64.encodeToString(sig, Base64.DEFAULT));
    }
    
    /**
     * Adds newline and prints to screen
     */
    private void logToScreen(String s) {
    	// Check if we're at the bottom
    	mText.setText(mText.getText() + "*** " + s + "\n");
    	// Scroll it to the bottom if we're
    }
}

